#
# Localization file parser
# Author: Maciej Winkler
#

import configparser, os

class Localization():
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.language = "English"
        pathToFile = os.path.join("l10n", "en.lng")

        self.config.read_file(open(pathToFile))

    def getText(self, key):
        return self.config.get(self.language, key)

l10n = Localization()