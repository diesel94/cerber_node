#
# Usage: python3 Main.py -s <hash> <options>
#

import sys

from Localization import l10n
from CharactersSet import CharacterSet
from Cracker import Cracker

class Main():
    hash = ""
    charSet = CharacterSet()

    def __init__(self):
        self.argv = sys.argv
        self.maxLen = 8
        self.verbose = False
        self.startPassword = []

    def ProcessSwitches(self):
        if len(self.argv) < 2:
            print(l10n.getText("noOptions"))
            return False

        # Display help
        if "-h" in self.argv:
            print(l10n.getText("help"))
            return False

        # Get hash value
        if "-H" in self.argv:
            index = self.argv.index("-H") + 1 # index of hash value
            self.hash = self.argv[index]
        else:
            print(l10n.getText("usage"))
            return False

        # Set maximum password length
        if "-P" in self.argv:
            index = self.argv.index("-P") + 1 # index of hash value
            self.maxLen = int(self.argv[index])

        # Toggle uppercase
        if "-U" in self.argv:
            self.charSet.SetComplexity("uppercase", True)

        # Toggle numbers
        if "-N" in self.argv:
            self.charSet.SetComplexity("numbers", True)

        # Toggle special characters
        if "-S" in self.argv:
            self.charSet.SetComplexity("specials", True)

        # Toggle verbose mode
        if "-v" in self.argv:
            self.verbose = True

        # Set start password
        if "-B" in self.argv:
            index = self.argv.index("-B") + 1 # index of hash value
            self.startPassword = list(self.argv[index])

        return True

    def main(self):
        if not self.ProcessSwitches():
            return # execution arguments were wrong

        if self.verbose:
            print(l10n.getText("welcome"))

        cracker = Cracker(self.hash, self.charSet.Get(), self.maxLen)
        cracked = cracker.Crack(self.startPassword)

        if not cracked:
            if self.verbose:
                print(l10n.getText("failure"))
        else:
            if self.verbose:
                print(l10n.getText("cracked"))
                print(self.hash + " = ", end='')
            print(cracked, end='')




if __name__ == "__main__":
    mainClass = Main()
    mainClass.main()
