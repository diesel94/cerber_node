#
# Characters set for cracker
# Just instatiate this class, SetComplexity() and execute "Get()"
#

import string

class CharacterSet():
    complexity = {
        "lowercase":True,
        "uppercase":False,
        "numbers":False,
        "specials":False
    }
    set = ""

    def SetComplexity(self, key, value):
        self.complexity[key] = value

    def AddLowercase(self):
        return string.ascii_lowercase

    def AddUppercase(self):
        return string.ascii_uppercase

    def AddNumbers(self):
        set = ""
        for i in range(10):
            set += str(i)
        return set

    def AddSpecials(self):
        return "][?/<~#`!@$%^&*()+=}|:\";',>{. "

    def SetCharacters(self):
        self.set = ""

        if self.complexity["lowercase"]:
            self.set += self.AddLowercase()
        if self.complexity["uppercase"]:
            self.set += self.AddUppercase()
        if self.complexity["numbers"]:
            self.set += self.AddNumbers()
        if self.complexity["specials"]:
            self.set += self.AddSpecials()

    def Get(self):
        if self.set == "":
            self.SetCharacters()
        return self.set