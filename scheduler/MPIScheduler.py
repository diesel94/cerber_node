__author__ = 'Maciek Walaszczyk'

from mpi4py import MPI
import subprocess


class MPIScheduler:
    def __init__(self, character_set, worker_argv):
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.size = self.comm.Get_size()
        self.character_set = character_set
        self.worker_argv = worker_argv
        self.work_offset = self.rank - 1

    def get_work(self):
        if self.work_offset < len(self.character_set):
            argv = self.worker_argv
            argv[0] = 'Main.py'
            argv = ['python3'] + argv
            argv.append('-B')
            argv.append(self.character_set[self.work_offset])
            self.work_offset += self.size
            return argv
        else:
            return []

    def run_master(self):
        result = self.comm.recv()
        if not result == '':
            return result


    def run_slave(self):
        work = self.get_work()
        if len(work) > 0:
            result = subprocess.check_output(work)
            result = result.decode('ascii')
            if result == '':
                self.run_slave()
            else:
                self.comm.send(result, 0)

    def run(self):
        if self.rank == 0:
            cracked = self.run_master()
            print(cracked)
            MPI.Finalize()
        else:
            self.run_slave()
