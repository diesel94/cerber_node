#!/usr/bin/python3
__author__ = 'Maciek Walaszczyk'

import sys
from MPIScheduler import MPIScheduler
from CharactersSet import CharacterSet


class Main:
    def __init__(self):
        self.argv = sys.argv
        self.character_set = CharacterSet()
        self.hash = ""
        self.max_len = 8

    def process_switches(self):
        # Get hash value
        if "-H" in self.argv:
            index = self.argv.index("-H") + 1  # index of hash value
            self.hash = self.argv[index]
        else:
            return False

        # Set maximum password length
        if "-P" in self.argv:
            index = self.argv.index("-P") + 1  # index of hash value
            self.max_len = int(self.argv[index])

        # Toggle uppercase
        self.character_set.SetComplexity("uppercase", "-U" in self.argv)

        # Toggle numbers
        self.character_set.SetComplexity("numbers", "-N" in self.argv)

        # Toggle special characters
        self.character_set.SetComplexity("specials", "-S" in self.argv)

        return True

    def main(self):
        if not self.process_switches():
            return

        char_set = self.character_set.Get()

        scheduler = MPIScheduler(char_set, self.argv)
        scheduler.run()


if __name__ == '__main__':
    Main().main()
