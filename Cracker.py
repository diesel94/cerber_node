#
# Brute-force password cracker algorithm
# Just instantiate this class and execute Crack(), it will return found password or False
#

import hashlib

class Cracker():
    def __init__(self, hash, charSet, maxLength):
        self.hash = hash
        self.charSet = charSet
        self.maxPassLen = maxLength

    # Return string out of an array of characters
    def ArrayToString(self, arr):
        joined = ""
        for char in arr:
            joined += char

        return joined

    # For given array of characters, calculate md5 hash and compare it to original one
    def CheckPassword(self, password):
        strPass = self.ArrayToString(password)
        encoded = bytes(strPass, 'ascii')
        hashed = hashlib.md5(encoded).hexdigest()

        if hashed == self.hash:
            return True
        return False

    # Recursive brute force algorithm
    def BruteForce(self, password, pos, length):
        for character in self.charSet:
            password[pos] = character
            if pos >= length-1:
                if self.CheckPassword(password):
                    return True
            else:
                if self.BruteForce(password, pos+1, length):
                    return True

    # Start password cracking. Set list startPassword to start computation from this password. One char per element
    def Crack(self, startPassword=[]):
        password = startPassword
        startPos = len(password)
        for i in range(self.maxPassLen):
            password.append('')
            if self.BruteForce(password, startPos, i+1):
                return self.ArrayToString(password)

        return False